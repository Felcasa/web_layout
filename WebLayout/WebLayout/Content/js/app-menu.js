! function (a, e, m) {
    "use strict";
    m.app = m.app || {};
    var u = m("body"),
        d = m(a),
        l = m('div[data-menu="menu-wrapper"]').html(),
        r = m('div[data-menu="menu-wrapper"]').attr("class");
    m.app.menu = {
        expanded: null,
        collapsed: null,
        hidden: null,
        container: null,
        horizontalMenu: !1,
        manualScroller: {
            obj: null,
            init: function () {
                var e = m(".main-menu").hasClass("menu-dark") ? "light" : "dark";
                this.obj = m(".main-menu-content").perfectScrollbar({
                    suppressScrollX: !0,
                    theme: e
                })
            },
            update: function () {
                if (this.obj) {
                    var e;
                    if (!0 === m(".main-menu").data("scroll-to-active")) e = 0 < m(".main-menu-content").find("li.active").parents("li").length ? m(".main-menu-content").find("li.active").parents("li").last().position() : m(".main-menu-content").find("li.active").position(), setTimeout(function () {
                        void 0 !== e && m.app.menu.container.stop().animate({
                            scrollTop: e.top
                        }, 300), m(".main-menu").data("scroll-to-active", "false")
                    }, 300);
                    m(".main-menu-content").perfectScrollbar("update")
                }
            },
            enable: function () {
                this.init()
            },
            disable: function () {
                this.obj && m(".main-menu-content").perfectScrollbar("destroy")
            },
            updateHeight: function () {
                "vertical-menu" != u.data("menu") && "vertical-overlay-menu" != u.data("menu") || !m(".main-menu").hasClass("menu-fixed") || (m(".main-menu-content").css("height", m(a).height() - m(".header-navbar").height() - m(".main-menu-header").outerHeight() - m(".main-menu-footer").outerHeight()), this.update())
            }
        },
        mMenu: {
            obj: null,
            init: function () {
                m(".main-menu").mmenu({
                    slidingSubmenus: !0,
                    offCanvas: !1,
                    counters: !1,
                    navbar: {
                        title: ""
                    },
                    navbars: [{
                        position: "top",
                        content: ["searchfield"]
                    }],
                    searchfield: {
                        resultsPanel: !0
                    },
                    setSelected: {
                        parent: !0
                    }
                }, {
                        classNames: {
                            divider: "navigation-header",
                            selected: "active"
                        },
                        searchfield: {
                            clear: !0
                        }
                    }), m("a.mm-next").addClass("mm-fullsubopen"), this.obj = m(".main-menu").data("mmenu")
            },
            enable: function () {
                this.init()
            },
            disable: function () { }
        },
        init: function (e) {
            if (0 < m(".main-menu-content").length) {
                this.container = m(".main-menu-content");
                var n = "";
                !0 === e && (n = "collapsed"), this.change(n)
            } else this.drillDownMenu()
        },
        drillDownMenu: function (e) {
            m(".drilldown-menu").length && ("sm" == e || "xs" == e ? "true" == m("#navbar-mobile").attr("aria-expanded") && m(".drilldown-menu").slidingMenu({
                backLabel: !0
            }) : m(".drilldown-menu").slidingMenu({
                backLabel: !0
            }))
        },
        change: function (e) {
            var n = Unison.fetch.now();
            this.reset();
            var a, t, i = u.data("menu");
            if (n) switch (n.name) {
                case "xl":
                case "lg":
                    "vertical-overlay-menu" === i ? this.hide() : "vertical-compact-menu" === i ? this.open() : "horizontal-menu" === i && "lg" == n.name ? this.collapse() : "collapsed" === e ? this.collapse(e) : this.expand();
                    break;
                case "md":
                    "vertical-overlay-menu" === i || "vertical-mmenu" === i ? this.hide() : "vertical-compact-menu" === i ? this.open() : this.collapse();
                    break;
                case "sm":
                case "xs":
                    this.hide()
            }
            "vertical-menu" !== i && "vertical-compact-menu" !== i && "vertical-content-menu" !== i || this.toOverlayMenu(n.name), u.is(".horizontal-layout") && !u.hasClass(".horizontal-menu-demo") && (this.changeMenu(n.name), m(".menu-toggle").removeClass("is-active")), "horizontal-menu" != i && this.drillDownMenu(n.name), "xl" == n.name && (m('body[data-open="hover"] .dropdown').on("mouseenter", function () {
                m(this).hasClass("show") ? m(this).removeClass("show") : m(this).addClass("show")
            }).on("mouseleave", function (e) {
                m(this).removeClass("show")
            }), m('body[data-open="hover"] .dropdown a').on("click", function (e) {
                if ("horizontal-menu" == i && m(this).hasClass("dropdown-toggle")) return !1
            })), m(".header-navbar").hasClass("navbar-brand-center") && m(".header-navbar").attr("data-nav", "brand-center"), "sm" == n.name || "xs" == n.name ? m(".header-navbar[data-nav=brand-center]").removeClass("navbar-brand-center") : m(".header-navbar[data-nav=brand-center]").addClass("navbar-brand-center"), m("ul.dropdown-menu [data-toggle=dropdown]").on("click", function (e) {
                0 < m(this).siblings("ul.dropdown-menu").length && e.preventDefault(), e.stopPropagation(), m(this).parent().siblings().removeClass("show"), m(this).parent().toggleClass("show")
            }), "horizontal-menu" == i && ("sm" == n.name || "xs" == n.name ? m(".menu-fixed").length && m(".menu-fixed").unstick() : m(".navbar-fixed").length && m(".navbar-fixed").sticky()), "vertical-menu" !== i && "vertical-overlay-menu" !== i && "vertical-content-menu" !== i || (jQuery.expr[":"].Contains = function (e, n, a) {
                return 0 <= (e.textContent || e.innerText || "").toUpperCase().indexOf(a[3].toUpperCase())
            }, a = m("#main-menu-navigation"), t = m(".menu-search"), m(t).change(function () {
                var e = m(this).val();
                if (e) {
                    m(".navigation-header").hide(), m(a).find("li a:not(:Contains(" + e + "))").hide().parent().hide();
                    var n = m(a).find("li a:Contains(" + e + ")");
                    n.parent().hasClass("has-sub") ? (n.show().parents("li").show().addClass("open").closest("li").children("a").show().children("li").show(), 0 < n.siblings("ul").length && n.siblings("ul").children("li").show().children("a").show()) : n.show().parents("li").show().addClass("open").closest("li").children("a").show()
                } else m(".navigation-header").show(), m(a).find("li a").show().parent().show().removeClass("open");
                return m.app.menu.manualScroller.update(), !1
            }).keyup(function () {
                m(this).change()
            }))
        },
        transit: function (e, n) {
            var a = this;
            u.addClass("changing-menu"), e.call(a), u.hasClass("vertical-layout") && (u.hasClass("menu-open") || u.hasClass("menu-expanded") ? (m(".menu-toggle").addClass("is-active"), "vertical-menu" !== u.data("menu") && "vertical-content-menu" !== u.data("menu") || m(".main-menu-header") && m(".main-menu-header").show()) : (m(".menu-toggle").removeClass("is-active"), "vertical-menu" !== u.data("menu") && "vertical-content-menu" !== u.data("menu") || m(".main-menu-header") && m(".main-menu-header").hide())), setTimeout(function () {
                n.call(a), u.removeClass("changing-menu"), a.update()
            }, 500)
        },
        open: function () {
            u.is(".vertical-mmenu") && this.mMenu.enable(), this.transit(function () {
                u.removeClass("menu-hide menu-collapsed").addClass("menu-open"), this.hidden = !1, this.expanded = !0
            }, function () {
                m(".main-menu").hasClass("menu-native-scroll") || u.is(".vertical-mmenu") || !m(".main-menu").hasClass("menu-fixed") || (this.manualScroller.enable(), m(".main-menu-content").css("height", m(a).height() - m(".header-navbar").height() - m(".main-menu-header").outerHeight() - m(".main-menu-footer").outerHeight()))
            })
        },
        hide: function () {
            u.is(".vertical-mmenu") && this.mMenu.disable(), this.transit(function () {
                u.removeClass("menu-open menu-expanded").addClass("menu-hide"), this.hidden = !0, this.expanded = !1
            }, function () {
                m(".main-menu").hasClass("menu-native-scroll") || u.is(".vertical-mmenu") || !m(".main-menu").hasClass("menu-fixed") || this.manualScroller.enable()
            })
        },
        expand: function () {
            !1 === this.expanded && this.transit(function () {
                u.removeClass("menu-collapsed").addClass("menu-expanded"), this.collapsed = !1, this.expanded = !0
            }, function () {
                u.is(".vertical-mmenu") ? this.mMenu.enable() : m(".main-menu").hasClass("menu-native-scroll") || "vertical-mmenu" == u.data("menu") || "horizontal-menu" == u.data("menu") ? this.manualScroller.disable() : m(".main-menu").hasClass("menu-fixed") && this.manualScroller.enable(), "vertical-menu" == u.data("menu") && m(".main-menu").hasClass("menu-fixed") && m(".main-menu-content").css("height", m(a).height() - m(".header-navbar").height() - m(".main-menu-header").outerHeight() - m(".main-menu-footer").outerHeight())
            })
        },
        collapse: function (e) {
            !1 === this.collapsed && this.transit(function () {
                u.removeClass("menu-expanded").addClass("menu-collapsed"), this.collapsed = !0, this.expanded = !1
            }, function () {
                "vertical-content-menu" == u.data("menu") && this.manualScroller.disable(), "horizontal-menu" == u.data("menu") && u.hasClass("vertical-overlay-menu") && m(".main-menu").hasClass("menu-fixed") && this.manualScroller.enable(), "vertical-menu" == u.data("menu") && m(".main-menu").hasClass("menu-fixed") && m(".main-menu-content").css("height", m(a).height() - m(".header-navbar").height())
            })
        },
        toOverlayMenu: function (e) {
            var n = u.data("menu");
            "sm" == e || "xs" == e ? (u.hasClass(n) && u.removeClass(n).addClass("vertical-overlay-menu"), "vertical-content-menu" == n && m(".main-menu").addClass("menu-fixed")) : (u.hasClass("vertical-overlay-menu") && u.removeClass("vertical-overlay-menu").addClass(n), "vertical-content-menu" == n && m(".main-menu").removeClass("menu-fixed"))
        },
        changeMenu: function (e) {
            m('div[data-menu="menu-wrapper"]').html(""), m('div[data-menu="menu-wrapper"]').html(l);
            var n = m('div[data-menu="menu-wrapper"]'),
                a = (m('div[data-menu="menu-container"]'), m('ul[data-menu="menu-navigation"]')),
                t = m('li[data-menu="megamenu"]'),
                i = m("li[data-mega-col]"),
                s = m('li[data-menu="dropdown"]'),
                o = m('li[data-menu="dropdown-submenu"]');
            "sm" == e || "xs" == e ? (u.removeClass(u.data("menu")).addClass("vertical-layout vertical-overlay-menu fixed-navbar"), m("nav.header-navbar").addClass("fixed-top"), n.removeClass().addClass("main-menu menu-light menu-fixed menu-shadow"), a.removeClass().addClass("navigation navigation-main"), t.removeClass("dropdown mega-dropdown").addClass("has-sub"), t.children("ul").removeClass(), i.each(function (e, n) {
                m(n).find(".mega-menu-sub").find("li").has("ul").addClass("has-sub");
                var a = m(n).children().first(),
                    t = "";
                a.is("h6") && (t = a.html(), a.remove(), m(n).prepend('<a href="#">' + t + "</a>").addClass("has-sub mega-menu-title"))
            }), t.find("a").removeClass("dropdown-toggle"), t.find("a").removeClass("dropdown-item"), s.removeClass("dropdown").addClass("has-sub"), s.find("a").removeClass("dropdown-toggle nav-link"), s.children("ul").find("a").removeClass("dropdown-item"), s.find("ul").removeClass("dropdown-menu"), o.removeClass().addClass("has-sub"), m.app.nav.init(), m("ul.dropdown-menu [data-toggle=dropdown]").on("click", function (e) {
                e.preventDefault(), e.stopPropagation(), m(this).parent().siblings().removeClass("open"), m(this).parent().toggleClass("open")
            })) : (u.removeClass("vertical-layout vertical-overlay-menu fixed-navbar").addClass(u.data("menu")), m("nav.header-navbar").removeClass("fixed-top"), n.removeClass().addClass(r), this.drillDownMenu(e), m("a.dropdown-item.nav-has-children").on("click", function () {
                event.preventDefault(), event.stopPropagation()
            }), m("a.dropdown-item.nav-has-parent").on("click", function () {
                event.preventDefault(), event.stopPropagation()
            }))
        },
        toggle: function () {
            var e = Unison.fetch.now(),
                n = (this.collapsed, this.expanded),
                a = this.hidden,
                t = u.data("menu");
            switch (e.name) {
                case "xl":
                case "lg":
                case "md":
                    !0 === n ? "vertical-compact-menu" == t || "vertical-mmenu" == t || "vertical-overlay-menu" == t ? this.hide() : this.collapse() : "vertical-compact-menu" == t || "vertical-mmenu" == t || "vertical-overlay-menu" == t ? this.open() : this.expand();
                    break;
                case "sm":
                case "xs":
                    !0 === a ? this.open() : this.hide()
            }
            this.drillDownMenu(e.name)
        },
        update: function () {
            this.manualScroller.update()
        },
        reset: function () {
            this.expanded = !1, this.collapsed = !1, this.hidden = !1, u.removeClass("menu-hide menu-open menu-collapsed menu-expanded")
        }
    }, m.app.nav = {
        container: m(".navigation-main"),
        initialized: !1,
        navItem: m(".navigation-main").find("li").not(".navigation-category"),
        config: {
            speed: 300
        },
        init: function (e) {
            this.initialized = !0, m.extend(this.config, e), u.is(".vertical-mmenu") || this.bind_events()
        },
        bind_events: function () {
            var s = this;
            m(".navigation-main").on("mouseenter.app.menu", "li", function () {
                var e = m(this);
                if (m(".hover", ".navigation-main").removeClass("hover"), u.hasClass("menu-collapsed") || "vertical-compact-menu" == u.data("menu") && !u.hasClass("vertical-overlay-menu")) {
                    m(".main-menu-content").children("span.menu-title").remove(), m(".main-menu-content").children("a.menu-title").remove(), m(".main-menu-content").children("ul.menu-content").remove();
                    var n, a, t, i = e.find("span.menu-title").clone();
                    if (e.hasClass("has-sub") || (n = e.find("span.menu-title").text(), a = e.children("a").attr("href"), "" !== n && ((i = m("<a>")).attr("href", a), i.attr("title", n), i.text(n), i.addClass("menu-title"))), t = e.css("border-top") ? 0 < s.GetIEVersion() ? e.offset().top + parseInt(e.css("border-top"), 10) : e.position().top + parseInt(e.css("border-top"), 10) : 0 < s.GetIEVersion() ? e.offset().top : e.position().top, "vertical-compact-menu" !== u.data("menu") && i.appendTo(".main-menu-content").css({
                        position: "fixed",
                        top: t
                    }), e.hasClass("has-sub") && e.hasClass("nav-item")) {
                        e.children("ul:first");
                        s.adjustSubmenu(e)
                    }
                }
                e.addClass("hover")
            }).on("mouseleave.app.menu", "li", function () { }).on("active.app.menu", "li", function (e) {
                m(this).addClass("active"), e.stopPropagation()
            }).on("deactive.app.menu", "li.active", function (e) {
                m(this).removeClass("active"), e.stopPropagation()
            }).on("open.app.menu", "li", function (e) {
                var n = m(this);
                if (n.addClass("open"), s.expand(n), m(".main-menu").hasClass("menu-collapsible")) return !1;
                n.siblings(".open").find("li.open").trigger("close.app.menu"), n.siblings(".open").trigger("close.app.menu"), e.stopPropagation()
            }).on("close.app.menu", "li.open", function (e) {
                var n = m(this);
                n.removeClass("open"), s.collapse(n), e.stopPropagation()
            }).on("click.app.menu", "li", function (e) {
                var n = m(this);
                n.is(".disabled") ? e.preventDefault() : u.hasClass("menu-collapsed") || "vertical-compact-menu" == u.data("menu") && n.is(".has-sub") && !u.hasClass("vertical-overlay-menu") ? e.preventDefault() : n.has("ul") ? n.is(".open") ? n.trigger("close.app.menu") : n.trigger("open.app.menu") : n.is(".active") || (n.siblings(".active").trigger("deactive.app.menu"), n.trigger("active.app.menu")), e.stopPropagation()
            }), m(".main-menu-content").on("mouseleave", function () {
                (u.hasClass("menu-collapsed") || "vertical-compact-menu" == u.data("menu")) && (m(".main-menu-content").children("span.menu-title").remove(), m(".main-menu-content").children("a.menu-title").remove(), m(".main-menu-content").children("ul.menu-content").remove()), m(".hover", ".navigation-main").removeClass("hover")
            }), m(".navigation-main li.has-sub > a").on("click", function (e) {
                e.preventDefault()
            }), m("ul.menu-content").on("click", "li", function (e) {
                var n = m(this);
                if (n.is(".disabled")) e.preventDefault();
                else if (n.has("ul"))
                    if (n.is(".open")) n.removeClass("open"), s.collapse(n);
                    else {
                        if (n.addClass("open"), s.expand(n), m(".main-menu").hasClass("menu-collapsible")) return !1;
                        n.siblings(".open").find("li.open").trigger("close.app.menu"), n.siblings(".open").trigger("close.app.menu"), e.stopPropagation()
                    } else n.is(".active") || (n.siblings(".active").trigger("deactive.app.menu"), n.trigger("active.app.menu"));
                e.stopPropagation()
            })
        },
        GetIEVersion: function () {
            var e = a.navigator.userAgent,
                n = e.indexOf("MSIE");
            return 0 < n ? parseInt(e.substring(n + 5, e.indexOf(".", n))) : navigator.userAgent.match(/Trident\/7\./) ? 11 : 0
        },
        adjustSubmenu: function (e) {
            var n, a, t, i, s, o, l = e.children("ul:first"),
                r = l.clone(!0);
            m(".main-menu-header").height(), n = 0 < this.GetIEVersion() ? e.offset().top : e.position().top, t = d.height() - m(".header-navbar").height(), s = 0, l.height(), 0 < parseInt(e.css("border-top"), 10) && (s = parseInt(e.css("border-top"), 10)), i = t - n - e.height() - 30, o = m(".main-menu").hasClass("menu-dark") ? "light" : "dark", "vertical-compact-menu" === u.data("menu") ? (a = n + s, i = t - n - 30) : "vertical-content-menu" === u.data("menu") ? (a = n + e.height() + s, i = t - m(".content-header").height() - e.height() - n - 60) : a = n + e.height() + s, "vertical-content-menu" == u.data("menu") ? r.addClass("menu-popout").appendTo(".main-menu-content").css({
                top: a,
                position: "fixed"
            }) : (r.addClass("menu-popout").appendTo(".main-menu-content").css({
                top: a,
                position: "fixed",
                "max-height": i
            }), m(".main-menu-content > ul.menu-content").perfectScrollbar({
                theme: o
            }))
        },
        collapse: function (e, n) {
            e.children("ul").show().slideUp(m.app.nav.config.speed, function () {
                m(this).css("display", ""), m(this).find("> li").removeClass("is-shown"), n && n(), m.app.nav.container.trigger("collapsed.app.menu")
            })
        },
        expand: function (e, n) {
            var a = e.children("ul"),
                t = a.children("li").addClass("is-hidden");
            a.hide().slideDown(m.app.nav.config.speed, function () {
                m(this).css("display", ""), n && n(), m.app.nav.container.trigger("expanded.app.menu")
            }), setTimeout(function () {
                t.addClass("is-shown"), t.removeClass("is-hidden")
            }, 0)
        },
        refresh: function () {
            m.app.nav.container.find(".open").removeClass("open")
        }
    }
}(window, document, jQuery);