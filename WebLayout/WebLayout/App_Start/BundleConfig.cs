﻿using System.Web;
using System.Web.Optimization;

namespace WebLayout
{
    public class BundleConfig
    {
        // Para obter mais informações sobre o agrupamento, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/css/css.css",
                "~/Content/css/bootstrap.min.css",
                "~/Content/css/style.min.css",
                "~/Content/css/font-awesome.min.css",
                "~/Content/css/style.min.css",
                "~/Content/css/flag-icon.min.css",
                "~/Content/css/pace.css",
                "~/Content/css/jquery-jvectormap-2.0.3.css",
                "~/Content/css/morris.css",
                "~/Content/css/unslider.css",
                "~/Content/css/climacons.min.css",
                "~/Content/css/bootstrap-extended.min.css",
                "~/Content/css/colors.min.css",
                "~/Content/css/components.min.css",
                "~/Content/css/vertical-menu.min.css",
                "~/Content/css/palette-gradient.min.css",
                "~/Content/css/palette-gradient.min.css",
                "~/Content/css/clndr.min.css",
                "~/Content/css/style.min.css"));


            bundles.Add(new StyleBundle("~/Content/icons").Include(
               "~/Content/icons/css/all.min.css"));


            bundles.Add(new ScriptBundle("~/Content/js").Include(
                 "~/Content/js/vendor.js",
                 "~/Content/js/app-menu.js",
                 "~/Content/js/app.js"));
        }
    }
}